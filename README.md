# Photography Zine Project 

As a lover of photography, I like to create booklets, called zines, to print and showcase my best clicks. 

This time, I made a git project out of it.

----
## Camera
* Nexus 6P
* OnePlus One

----
## Softwares
###Post Processing
* Darktable
* Snapseed

### Booklet Design
* Adobe Illustrator
* Adobe InDesign

---
## Other Projects
[BitBucket](https://bitbucket.org/abdullahalam/)
